<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Start page</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <script src="../js/bootstrap.min.js"></script>
</head>
<body>
<form name="Simple" action="show" method="GET">
    <button type="submit" class="btn btn-default" name="button" value="dom">
        dom
    </button>
    <button type="submit" class="btn btn-default" name="button" value="sax">
        SAX
    </button>
    <button type="submit" class="btn btn-default" name="button" value="STAX">
        StAX
    </button>
</form>
</body>
</html>
