package by.bsuir.epam.project.models;

/**
 * Created by anton on 19/11/15.
 */
public class Card {
    private String theme;
    private String type;
    private boolean send;
    private String country;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    private int year;
    private String author;
    private String valuable;

    public Card(){}

    public Card(String thema, String type, boolean send, String country, int year, String author, String valuable){
        this.theme = thema;
        this.type = type;
        this.send = send;
        this.country = country;
        this.year = year;
        this.author = author;
        this.valuable =valuable;
    }

    public Card(String thema, String type, boolean send, String country, int year, String valuable){
        this.theme = thema;
        this.type = type;
        this.send = send;
        this.country = country;
        this.year = year;
        this.author = "unknown";
        this.valuable =valuable;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String thema) {
        this.theme = thema;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSend() {
        return send;
    }

    public void setSend(boolean send) {
        this.send = send;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getValuable() {
        return valuable;
    }

    public void setValuable(String valuable) {
        this.valuable = valuable;
    }
}
