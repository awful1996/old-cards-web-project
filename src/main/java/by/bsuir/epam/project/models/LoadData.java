package by.bsuir.epam.project.models;

import by.bsuir.epam.project.parsers.AbstractCardsBuilder;
import by.bsuir.epam.project.parsers.CardBuilderFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Антон on 11/20/2015.
 */
public class LoadData {

    private static List<Card> cards = new ArrayList<Card>();

    public static int amountOfCard(){
        return cards.size();
    }

    public static List<Card> load(String type){
        CardBuilderFactory builder = new CardBuilderFactory();
        AbstractCardsBuilder card = builder.createStudentBuilder(type);
        card.buildSetCards("../webapps/ROOT/data/cards.xml");
        cards = card.getCards();
        return cards;
    }

    public static List<Card> getSubCards(int firstIndex, int secondIndex){
        return cards.subList(firstIndex,secondIndex);
    }
}
