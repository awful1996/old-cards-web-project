package by.bsuir.epam.project.parsers;

import java.util.*;

import by.bsuir.epam.project.models.Card;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by Антон on 11/20/2015.
 */
public class CardHandler extends DefaultHandler {
    private List<Card> cards;

    private Card current = null;
    private CardEnum currentEnum = null;
    private EnumSet<CardEnum> withText;
    private final String UNKNOWN_AUTHOR = "unknown";


    public CardHandler() {
        cards = new ArrayList<Card>();
        withText = EnumSet.range(CardEnum.THEME, CardEnum.TYPE);
    }

    public List<Card> getCards() {
        return cards;
    }

    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        if (CardEnum.CARD.getValue().equals(localName)) {
            current = new Card();
            current.setSend(Boolean.valueOf(attrs.getValue(1)));
            if (attrs.getLength() == 3) {
                current.setAuthor(attrs.getValue(2));
            }
            else{
                current.setAuthor(UNKNOWN_AUTHOR);
            }
        } else {
            CardEnum temp = CardEnum.valueOf(localName.toUpperCase());
            if (withText.contains(temp)) {
                currentEnum = temp;
            }
        }
    }

    public void endElement(String uri, String localName, String qName) {
        if (CardEnum.CARD.getValue().equals(localName)) {
            cards.add(current);
        }
    }

    public void characters(char[] ch, int start, int length) {
        String s = new String(ch, start, length).trim();
        if (currentEnum != null) {
            switch (currentEnum) {
                case THEME:
                    current.setTheme(s);
                    break;
                case YEAR:
                    current.setYear(Integer.parseInt(s));
                    break;
                case TYPE:
                    current.setType(s);
                    break;
                case COUNTRY:
                    current.setCountry(s);
                    break;
                case VALUABLE:
                    current.setValuable(s);
                    break;
                default:
                    throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
            }
        }
        currentEnum = null;
    }
}
