package by.bsuir.epam.project.parsers;

/**
 * Created by Антон on 11/20/2015.
 */
public enum CardEnum {
    OLD_CARDS("old_Cards"),
    CARD("card"),
    THEME("theme"),
    COUNTRY("country"),
    YEAR("year"),
    VALUABLE("valuable"),
    TYPE("type"),
    AUTHOR("author"),
    SEND("send");
    private String value;

    CardEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
