package by.bsuir.epam.project.parsers;

import by.bsuir.epam.project.models.Card;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by anton on 20/11/15.
 */
public class CardsSAXBuilder extends AbstractCardsBuilder {
    private static final Logger logger = Logger.getLogger(CardsSAXBuilder.class);

    private CardHandler cardHandler;
    private XMLReader reader;

    public CardsSAXBuilder() {
        cardHandler = new CardHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();

            reader.setContentHandler(cardHandler);

        } catch (SAXException e) {
            logger.error("ошибка SAX парсера: " + e);
        }
    }
    public CardsSAXBuilder (ArrayList<Card> cards) {
        super(cards);
    }

    @Override
    public void buildSetCards(String fileName) {
        try {
            reader.parse(fileName);
        } catch (SAXException e) {
            logger.error("ошибка SAX парсера: " + e);
        } catch (IOException e) {
            logger.error("ошибка I/О потока: " + e);
        }
        cards = cardHandler.getCards();
    }
}
