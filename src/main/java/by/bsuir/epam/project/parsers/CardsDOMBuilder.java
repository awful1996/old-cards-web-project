package by.bsuir.epam.project.parsers;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import by.bsuir.epam.project.models.Card;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Created by anton on 20/11/15.
 */
public class CardsDOMBuilder extends AbstractCardsBuilder {
    private final static Logger logger = Logger.getLogger(CardsDOMBuilder.class);
    private final String UNKNOWN_AUTHOR = "unknown";

    private DocumentBuilder docBuilder;
    public CardsDOMBuilder() {
        this.cards = new ArrayList<Card>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.error("Ошибка конфигурации парсера: " + e);
        }
    }

    public CardsDOMBuilder (ArrayList<Card> cards) {
        super(cards);
    }

    @Override
    public void buildSetCards(String fileName) {
        Document doc;
        try {
            doc = docBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList cardsList = root.getElementsByTagName(CardEnum.CARD.getValue());
            for (int i = 0; i < cardsList.getLength(); i++) {
                Element studentElement = (Element) cardsList.item(i);
                Card student = buildCard(studentElement);
                cards.add(student);
            }
        } catch (IOException e) {
            logger.error("File error or I/O error: " + e);
        } catch (SAXException e) {
            logger.error("Parsing failure: " + e);
        }
    }

    private Card buildCard(Element cardElement) {
        Card card = new Card();
        String name = cardElement.getAttribute(CardEnum.AUTHOR.getValue());
        if(name.length() == 0){
            card.setAuthor(UNKNOWN_AUTHOR);
        }
        else{
            card.setAuthor(name);
        }
        card.setSend(Boolean.valueOf(cardElement.getAttribute(CardEnum.SEND.getValue())));
        Integer year = Integer.parseInt(getElementTextContent( cardElement,CardEnum.YEAR.getValue()));
        card.setYear(year);
        card.setTheme(getElementTextContent(cardElement, CardEnum.THEME.getValue()));
        card.setCountry(getElementTextContent(cardElement, CardEnum.COUNTRY.getValue()));
        card.setType(getElementTextContent(cardElement, CardEnum.TYPE.getValue()));
        card.setValuable(getElementTextContent(cardElement, CardEnum.VALUABLE.getValue()));
        return card;
    }

    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }

}
