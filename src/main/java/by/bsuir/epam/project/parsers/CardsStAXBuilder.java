package by.bsuir.epam.project.parsers;

import by.bsuir.epam.project.models.Card;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class CardsStAXBuilder extends AbstractCardsBuilder{
    private static final Logger logger = Logger.getLogger(CardsStAXBuilder.class);
    private final String UNKNOWN_AUTHOR = "unknown";
    private XMLInputFactory inputFactory;

    public CardsStAXBuilder() {
        inputFactory = XMLInputFactory.newInstance();
    }

    public CardsStAXBuilder(List<Card> cards) {
        super(cards);
    }

    @Override
    public void buildSetCards(String fileName) {
        FileInputStream inputStream = null;
        XMLStreamReader reader;
        String name;
        try {
            inputStream = new FileInputStream(new File(fileName));
            reader = inputFactory.createXMLStreamReader(inputStream);

            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName().toString();
                    if (name.toString().equals(CardEnum.CARD.getValue())) {
                        Card st = buildCard(reader);
                        cards.add(st);
                    }
                }
            }
        } catch (XMLStreamException ex) {
            logger.error("StAX parsing error! " + ex.getMessage());
        } catch (FileNotFoundException ex) {
            logger.error("File " + fileName + " not found! " + ex);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                logger.error("Impossible close file " + fileName + " : " + e);
            }
        }

    }


    private Card buildCard(XMLStreamReader reader) throws XMLStreamException {
        Card card = new Card();
        String author = reader.getAttributeValue(null, CardEnum.AUTHOR.getValue());
        if(author == null) {
            card.setAuthor(UNKNOWN_AUTHOR);
        }
        else {
            card.setAuthor(author);
        }
        card.setSend(Boolean.valueOf(reader.getAttributeValue(null, CardEnum.SEND.getValue())));
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (CardEnum.valueOf(name.toUpperCase())) {
                        case THEME:
                            card.setTheme(getXMLText(reader));
                            break;
                        case COUNTRY:
                            card.setCountry(getXMLText(reader));
                            break;
                        case YEAR:
                            card.setYear(Integer.parseInt(getXMLText(reader)));
                            break;
                        case VALUABLE:
                            card.setValuable(getXMLText(reader));
                            break;
                        case TYPE:
                            card.setType(getXMLText(reader));
                            break;
                    } break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (CardEnum.valueOf(name.toUpperCase()) == CardEnum.CARD) {
                        return card;
                    }
                    break;
            }
        } throw new XMLStreamException("Unknown element in tag Student");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}