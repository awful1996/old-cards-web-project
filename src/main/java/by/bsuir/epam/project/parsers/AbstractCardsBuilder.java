package by.bsuir.epam.project.parsers;


import by.bsuir.epam.project.models.Card;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by anton on 20/11/15.
 */
public abstract class AbstractCardsBuilder {
    protected List<Card> cards;

    public AbstractCardsBuilder() {
        cards = new ArrayList<Card>();
    }

    public AbstractCardsBuilder(List<Card> cards) {
        this.cards = cards;
    }

    public List<Card> getCards() {
        return cards;
    }

    abstract public void buildSetCards(String fileName);
}
