package by.bsuir.epam.project.parsers;

import org.apache.log4j.Logger;

/**
 * Created by Антон on 11/20/2015.
 */
public class CardBuilderFactory {
    private static final Logger logger = Logger.getLogger(CardBuilderFactory.class);

    private enum TypeParser {SAX, STAX, DOM}

    public AbstractCardsBuilder createStudentBuilder(String typeParser) {
        TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
        switch (type) {
            case DOM:
                logger.info("Chosen DOM parser");
                return new CardsDOMBuilder();
            case STAX:
                logger.info("Chosen StAX parser");
                return new CardsStAXBuilder();
            case SAX:
                logger.info("Chosen SAX parser");
                return new CardsSAXBuilder();
            default:
                throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());
        }

    }
}
