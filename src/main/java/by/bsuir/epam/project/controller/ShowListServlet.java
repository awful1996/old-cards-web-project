package by.bsuir.epam.project.controller;

import by.bsuir.epam.project.models.Card;
import by.bsuir.epam.project.models.LoadData;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Created by anton on 19/11/15.
 */
@WebServlet("/show")
public class ShowListServlet extends HttpServlet {

    private final static Logger logger = Logger.getLogger(ShowListServlet.class);
    private final int RECORDS_ON_PAGE = 10;

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("doGet ShowListServlet");
        int page = 1;
        if(request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));
        if(request.getParameter("button") != null)
        {
            LoadData.load(request.getParameter("button"));
        }
        int amountOfPages = (int) Math.ceil(LoadData.amountOfCard() * 1.0 / RECORDS_ON_PAGE);
        List<Card> list;
        if(amountOfPages == page){
            list = LoadData.getSubCards((page-1)*RECORDS_ON_PAGE, LoadData.amountOfCard()-1);
        }
        else {
            list = LoadData.getSubCards((page - 1) * RECORDS_ON_PAGE, page * RECORDS_ON_PAGE);
        }
        request.setAttribute("cardList", list);
        request.setAttribute("amountOfPages", amountOfPages);
        request.setAttribute("currentPage", page);
        RequestDispatcher view = request.getRequestDispatcher("view/showPage.jsp");
        view.forward(request, response);
    }
}
