package by.bsuir.epam.project.controller;

import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by anton on 19/11/15.
 */
@WebServlet("/startProject")
public class StartServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(StartServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("view/startPage.jsp");
        logger.info("doGet StartServlet");
        view.forward(request, response);

    }
}