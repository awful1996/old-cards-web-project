<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <script src="../js/bootstrap.min.js"></script>
</head>
<head>
    <title>Old cards</title>
</head>
<body>
<div class="container">
    <h2>Old cards</h2>

    <table border="1" cellpadding="5" cellspacing="5" class="table table-striped">
        <tr>
            <td>Theme</td>
            <td>Type</td>
            <td>Send</td>
            <td>Country</td>
            <td>Year</td>
            <td>Author</td>
            <td>Valuable</td>
        </tr>

        <c:forEach var="card" items="${cardList}">
            <tr>
                <td>${card.theme}</td>
                <td>${card.type}</td>
                <td>${card.send}</td>
                <td>${card.country}</td>
                <td>${card.year}</td>
                <td>${card.author}</td>
                <td>${card.valuable}</td>
            </tr>
        </c:forEach>
    </table>

    <c:if test="${currentPage != 1}">
        <td><a href="/show?page=${currentPage - 1}">Previous</a></td>
    </c:if>

    <table border="1" cellpadding="5" cellspacing="5">
        <tr>
            <c:forEach begin="1" end="${amountOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <td>${i}</td>
                    </c:when>
                    <c:otherwise>
                        <td><a href="/show?page=${i}">${i}</a></td>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </tr>
    </table>

    <c:if test="${currentPage lt amountOfPages}">
        <td><a href="/show?page=${currentPage + 1}">Next</a></td>
    </c:if>

</div>
</body>
</html>
